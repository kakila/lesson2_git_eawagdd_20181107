# Documentation folder

This folder contains ascii files that are tracked by git.
Ascii files include Latex source files, plain txt files, xml files, etc.
Anything that is not a binary fomrat is well suited for tracking with git.

## Fortunes

The file [fortunes_extract.txt](fortunes_extract.txt) was extracted from the file
 [fortunes](https://github.com/bmc/fortunes/blob/master/fortunes), which is shared by
 Brian M. Clapper under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).
